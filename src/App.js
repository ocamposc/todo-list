import React, { Component } from 'react'
import Header from './components/Header'
import AddTask from './components/AddTask';
import ListTasks from './components/ListTasks';

import 'bootstrap/dist/css/bootstrap.min.css';

export default class App extends Component {
  state={
    tasks: []
  }

  componentDidMount(){
    const tasks = localStorage.getItem('tasks');
    if(tasks) {
      this.setState({
        tasks:JSON.parse(tasks)
      })
    }
  }

  componentDidUpdate(){
    localStorage.setItem('tasks', JSON.stringify(this.state.tasks));
  }

  createNewTask = newTask => {
    const tasks = [...this.state.tasks, newTask];
    this.setState({tasks})
  }

  deleteTask = id => {
    const stateTasks = [...this.state.tasks];
    const tasks = stateTasks.filter( task => task.id !== id );
    this.setState({tasks});
  }

  editTask = (id, value) => {
    const stateTasks = [...this.state.tasks];
    const tasks = stateTasks.filter( task => task.id === id );
    tasks[0].comment = value;
    this.setState({stateTasks});
  }



  render() {
    return (
      <div className="container">
        <Header 
           titulo='TODO List'/>

        <div className="contenido-principal">
          <AddTask
            createNewTask={this.createNewTask}
            />   

          <ListTasks 
            tasks={this.state.tasks}
            deleteTask={this.deleteTask}
            editTask={this.editTask}
            />
        </div>
      </div>
    )
  }
}
