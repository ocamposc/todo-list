import React, { Component } from 'react'

export default class Task extends Component {

    state = {
        edit : false,
        value : ''
    }

    handleEdit = () => {
        this.setState({edit : true});
    }

    handleSubmit = e => {
        e.preventDefault()

        this.props.editTask(this.props.task.id, this.state.value)

        this.setState({edit : false});
        this.setState({value:''});
    }

    handleChange = e => {
        this.setState({
            value : e.target.value
        })
    }

    render() {

        return (
            <div className="card mt-2 py-2">   
                <ul>
                    <li>
                        <h4 className="mt-0">{this.props.task.title}</h4>   
                    </li>
                    {this.state.edit ? <form onSubmit={this.handleSubmit}><input type="text" onChange={this.handleChange}/> <input type="submit" value="save"/> </form> : <h6 className="mt-0">{this.props.task.comment}</h6>}
                    {this.state.edit ? null: <button onClick={this.handleEdit}>edit</button>} 
                    <button onClick={() => this.props.deleteTask(this.props.task.id)}>delete</button> 
                </ul>

            </div>
        )
    }
}
