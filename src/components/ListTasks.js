import React, { Component } from 'react'
import Task from './Task'

export default class ListTasks extends Component {
    render() {
        return (
                <div >
                    {/* <div className="card-body"> */}
                        {this.props.tasks.map(task => (
                            <Task
                                key={task.id}
                                task={task}
                                deleteTask={this.props.deleteTask}
                                editTask={this.props.editTask}
                            />
                        ))}
                    {/* </div> */}
                </div>
        )
    }
}




