import React, { Component } from 'react'
import uuid from 'uuid';

const initialState = {
    task : {
        title:'',
        comment:'',
        active:true
    },
    error : false,
}

export default class AddTask extends Component {
    state = {...initialState}

    handleChange = e => {
        this.setState({
            task : {
                ...this.state.task,
                [e.target.name] : e.target.value
            }
        })
    }

    handleSubmit = e => {
        e.preventDefault();

        const {title} = this.state.task;

        if(title === ''){
            this.setState({error : true});
            return;
        }

        const newTask = {...this.state.task};
        newTask.id = uuid();

        this.props.createNewTask(newTask);

        this.setState({...initialState});
    }
    
    render() {

        return (
            <div className="card mt-2 py-2">
            <h3>Add New Task</h3>
                <div className="card-body">

                    <form
                        onSubmit={this.handleSubmit}>

                        <div className="form-group row">
                            <input 
                                type="text" 
                                name="title" 
                                id="title" 
                                placeholder="title" 
                                value={this.state.task.title}
                                onChange={this.handleChange}/>
                        </div>

                        <div className="form-group row">
                            <input 
                                type="text" 
                                name="comment" 
                                id="comment" 
                                placeholder="comment" 
                                value={this.state.task.comment}
                                onChange={this.handleChange}/>
                        </div>
                        <div className="form-group row">
                            <input 
                                type="submit"
                                value="Create"/>
                        </div>
                    </form>

                </div>
            </div>
        )
    }
}

