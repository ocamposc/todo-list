import React from 'react'

export default function Header({titulo}) {
    return (
        <header>
<h1 className="text-center">{titulo}</h1>
        </header>
    )
}
